public interface calculator {


    public double add(double a,double b);
    public float add(float a,float b);
    public float add(int a,float b);
    public int add(int a,int b);

    public double sub(double a,double b);
    public float sub(float a,float b);
    public float sub(int a,float b);
    public int sub(int a,int b);

    public double mul(double a,double b);
    public float mul(float a,float b);
    public float mul(int a, float b);
    public int mul(int a,int b);

    public double div(double a,double b);
    public float div(float a,float b);
    public float div(int a,float b);
    public int div(int a,int b);

    public double mod(double a,double b);
    public float mod(float a,float b);
    public float mod(int a,float b);
    public int mod(int a,int b);

}
